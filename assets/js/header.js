headerInit();

function headerInit() {

    var headerEl = document.querySelectorAll('.page-header');
    if (headerEl.length > 0) { //If there is at least one page header element
        //Init headroom js to allow the header to know when it is at the top of the page
        for (var i = 0; i < headerEl.length; i++) {
            var headroom = new Headroom(headerEl[i]);
            headroom.init();
        }
    }

    var headerNavToggleEl = document.querySelectorAll('.page-header__nav-toggle');
    if (headerNavToggleEl.length > 0) { //If there is at least one nav toggle button
        for (var i = 0; i < headerNavToggleEl.length; i++) {
            headerNavToggleEl[i].addEventListener('click', headerNavToggle);
        }
    }
}

function headerNavToggle(e) {
    var thisEl = e.currentTarget;
    thisEl.classList.toggle('is-active');
    thisEl.nextElementSibling.classList.toggle('is-active');
}
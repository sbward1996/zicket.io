modalInit(); 

function modalInit() {
    var modalEls = document.querySelectorAll('.modal-button');
    if (modalEls.length === 0)
        return;
    
    for (var i = 0; i < modalEls.length; i++)
        modalEls[i].addEventListener("click", buildModal);
}

function buildModal(e) {
    e.preventDefault;
    var thisEl = e.currentTarget;
    var iframeSrc = thisEl.getAttribute("data-iframe-src");
    if (iframeSrc == null || iframeSrc.length === 0)
        return;
    var modalEl = document.createElement('div');
    var modalWrapperEl = document.createElement('div');
    var iframeEl = document.createElement('iframe');
    var closeButtonEl = document.createElement("button");

    modalEl.classList.add("page-modal");
    modalWrapperEl.classList.add("page-modal__wrapper");
    closeButtonEl.classList.add("page-modal__close");
    closeButtonEl.innerText = "X";

    closeButtonEl.addEventListener('click', killModal);

    iframeEl.setAttribute("src", iframeSrc);

    modalWrapperEl.appendChild(iframeEl);
    modalWrapperEl.appendChild(closeButtonEl);
    modalEl.appendChild(modalWrapperEl);
    document.querySelector('body').appendChild(modalEl);
}

function killModal(e) {
    e.preventDefault();
    var thisEl = e.currentTarget;
    var modalEl = thisEl.closest('.page-modal'); //Look up the dom for .page-modal
    var modalParentEl = modalEl.parentElement;

    if (modalEl.length == 0)
        return;

    modalParentEl.removeChild(modalEl);
}
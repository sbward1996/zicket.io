newsletterInit(); 

function newsletterInit() {
    var newsletterFormEl = document.querySelectorAll('.newsletter-ajax-form');
    if (newsletterFormEl.length === 0)
        return;
    for (var i = 0; i < newsletterFormEl.length; i++)
        newsletterFormEl[i].addEventListener("submit", newsletterSubmit);
}

/*
 * Started, but ssl is not working on the site for testing
 */
function newsletterSubmit(e) {
    e.preventDefault();

    var thisEl = e.currentTarget;
    var formData = new FormData(thisEl); //Get form data ready for ajax
    
    fetch(
        "https://frontend-trial-api.qa.parallax.dev/api/newsletter",
        {
            method: "POST",
            body: (formData) // body data type must match "Content-Type" header
        }
    ).then(function(response) {
        return response.json();
    }).then(function(data) { //Dumb version due to time constraits
        if (data.success == true)
            alert("Thank you! You successfully signed up to our mailing list.");
        else
            alert(data.errors.join("|"));
    });
}
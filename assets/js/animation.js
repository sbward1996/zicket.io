heroInit();
window.addEventListener('scroll',parallaxEls);

function parallaxEls() {
    var imageEls = document.querySelectorAll('.content-row figure');
    if (imageEls.length == 0)
        return;

    /*
     * Quick dirty js parallax - Needs optimising
     */
    for (var i = 0; i < imageEls.length; i++) {
        var thisEl = imageEls[i];
        var imageEl = thisEl.querySelector('img');
        var pos = thisEl.getBoundingClientRect();
        var posToMove = pos.top * .1;

        imageEl.style.transform = "translateY("+posToMove+"px)";
    }
}

function heroInit() {
    var heroEl = document.querySelectorAll('.page-hero');
    if (heroEl.length === 0)
        return;
    for (var i = 0; i < heroEl.length; i++)
        heroEl[i].classList.add('active');
}
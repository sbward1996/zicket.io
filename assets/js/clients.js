clientsSliderInit();

function clientsSliderInit() {
    var clientsSliderEl = document.querySelectorAll('.clients-showcase__slider');
    if (clientsSliderEl.length == 0)
        return;

    for (var i = 0; i < clientsSliderEl.length; i++)
        new Glide(clientsSliderEl[i], {
            type: 'carousel',
            startAt: 0,
            perView: 4,
            peek: {
                before: 0,
                after: 50
            }
        }).mount()
}
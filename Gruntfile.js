module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'assets/css/style.min.css': 'assets/sass/main.scss',
                }
            }
        },

        autoprefixer: {
            dist: {
                files: {
                    'assets/css/style.min.css': 'assets/css/style.min.css'
                },
                options: {
                    map: true
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    'assets/js/dist/script.min.js': ['assets/js/lib/headroom.js','assets/js/lib/glide.min.js' , 'assets/js/header.js', 'assets/js/newsletter.js','assets/js/modal.js', 'assets/js/clients.js',"assets/js/animation.js"]
                }
            }
        },

        imagemin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'assets/images/',
                        src: ['assets/**/*.{png,jpg,gif}'],
                        dest: 'assets/images/'
                    }
                ]

            }
        },

        watch: {
            sass: {
                files: 'assets/sass/**',
                tasks: ['sass', 'autoprefixer'],
            },

            uglify: {
                files: 'assets/js/**',
                tasks: ['newer:uglify'],
            },

            imagemin: {
                files: 'assets/imgages/**',
                tasks: ['newer:imagemin'],
            }
        }
    });

    grunt.event.on('watch', function (action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask('default', ['sass', 'autoprefixer', 'newer:imagemin', 'newer:uglify']);
};